# NAME

xmpl - An example web application, with flexible deployment capabilities.

# VERSION

The version can be retrieved with option `--version`:

    $ xmpl --version

# USAGE

Running `xmpl` by itself will give you information about sub-commands.

# EXAMPLES

    # simple instance, in-memory key/value store, listen on port 3000
    xmpl daemon

    # change port to 3001, listen on all interfaces though
    xmpl daemon -l 'http:/*:3001'

    # use a JSON file store for persistence of key/value pairs
    KVSTORE=./repo.json xmpl

    # use a remote instance of xmpl as store for key/value pairs
    KVSTORE=http://host:port xmpl

# DESCRIPTION

This is an example web application that provides a few APIs for varius
testing reasons.

## Simple Key/Value Store

The application provies a simple API to manipulate a key/value store.
It is not meant to be a robust, production-grade key/value store - only
something to make a few tests.

There are three possible ways to store the key/value pairs:

- **in memory** - which pretty much tells everything;
- **on file** - if environment variable `KVSTORE` points to a file, it
  will be used to save a JSON-encoded version of the hash representing
  the key/value pairs;
- **remote** - if environment variable `KVSTORE` holds a URL, it is
  assumed to be another instance of `xmpl`, providing the API below.

This allows flexible deployment strategies, e.g. to test
frontend/backend architecture, or to make tests with some storage
backend.

The following API endpoints are available:

- `GET /kvs` returns a JSON-encoded string with an object that has key
  `kv` and the whole key/value store as a nested object;

- `POST /kvs` allows adding (or overwriting) a (new) key/value pair. In
  particular, the key is passed through parameter `key` and the value
  through parameter `value`;

- `DELETE /kvs/key-name` removes a key/value pair (if present)
  corresponding to key `key-name`;

- `GET /kvs/key-name` returns a JSON object with the key/value pair,
  respectively as values associated to keys `key` and `value`;

- `PUT /kvs/key-name` sets the value associated to key `key-name`. The
  value set is taken from the body of the request.

## Browser-oriented Web Application

It is possible to access the key/value pair store also through a
browser-based interface, providing the following simplified and
browser-friendly API:

- `GET /` gets a HTML page with a form to add new key/value pairs (or
  set existing ones), as well as a list of all key/value pairs currently
  stored and the possibility to delete each of them;
- `POST /` allows the forms in the page above to add/remove elements, as
  described.
- `GET /favicon.ico` returns a 32x32 PNG image to be displayed as the
  application's *icon*.

## Health Checks

To ease the deployment in [Kubernetes][] environments, it's possible to
invoke the `/healthz` endpoint. It is actually implemented as two
different API endpoints:

- `GET /healthz` returns HTTP status code `204` if everything is OK,
  `500` otherwise;
- `PUT /healthz` sets (possibly overrides) the health status.

By default, the health check just tries to save the key/value pairs
store to disk when the `KVSTORE` environment variable is set to a file;
otherwise, it is always successful.

It is anyway possible to override the health status with the `PUT`
interface:

- Setting to `0` in the body means that the health check will always
  fail;
- Setting to an empty value in the body resets the overriding and
  restores the check on the disk (if applicable);
- Setting to pretty much everything else in the body means that the
  health check will always succeed.

Leading/trailing spaces are removed before applying the rules above.

## Metrics

The application exposes a few *metrics* in a [Prometheus][]-compatible
format, to ease experimenting with this monitoring tool. The following
API is available:

- `GET /metrics` get the metrics for the `xmpl` instance

An example (possibly outdated) of output is the following:

```
# HELP life_time_seconds Time since start of process
# TYPE life_time_seconds counter
life_time_seconds 72
# HELP healthz_status Status of health (1 healthy, 0 unhealthy)
# TYPE healthz_status gauge
healthz_status 1
# HELP metrics_calls Calls to the /metrics endpoint
# TYPE metrics_calls counter
metrics_calls 42
# HELP random_d6 A random value from a regular 6-sided die
# TYPE random_d6 gauge
random_d6 3
# HELP kvstore_info Info on the key/value store (as labels)
# TYPE kvstore_info gauge
kvstore_info{kind=Remote} 1
```

Look at the code for the current content of this section.

## Page configuration

The application exposes an interface to customize the web page where it is
possible to add/visualize/remove key-value pairs. As of this version, it's
possible to only manipulate the stylesheet.

The following API is available:

- `GET /page-config` get the current page configuration, as
  a JSON-formatted object

The default configuration is the following:

```json
{"bgcolor":"#eeffee","font":"sans-serif","title":"Example Application"}
```

- `PUT /page-config` set the current page configuration, as
  a JSON-formatted object in the body.

The newly set JSON object overrides the previous one completely, except
that the keys in the default configuration above MUST always exist (they
are set back to the default value if they are missing or their value is
undefined/null).

The following keys are supported for affecting the page's appearance:

- `bgcolor`: the color of the body background;
- `font`: the font family used in the page;
- `title`: the title of the page;
- `css`: when present, it completely overrides the page's default
  cascading style sheet, so the values for `bgcolor` and `font` keys above
  will be ignored.

## Miscelanneous endpoints

The following additional API endpoints are available:

- `GET /date` returns a JSON-encoded object with key `now` and value set
  to a date in one of the several [ISO 8601][] formats (the most compact
  with date, time and timezone set to `Z`);

- `* /status/status-code` where `*` represent any valid HTTP verb, and
  `status-code` the HTTP status code you want back.

- `GET /sitemap` displays a HTML page listing the valid target and HTTP
  method combinations.

- `*` as a fallback, everything else will just return HTTP status code
  `404` with a short error message.

# OPTIONS

This is a [Mojoliious][]-based applications, so options might vary
depending on the underlying version of the web toolkit. Run `xmpl` to
get access to the actual options set available.

The only overriding to the rule above is:

- `--version` print the version of `xmpl` and return. This option MUST
  be the only one to be triggered correctly.

# ENVIRONMENT VARIABLES

- `KVSTORE` helps `xmpl` decide to use the in-memory (when not set or
  set to an empty string), a remote instance of `xmpl` (when set to an
  HTTP URL) or a local file (if the value can be used as a file name).

# DEPENDENCIES

Perl 5.24 or later. [Mojolicious][].

# BUGS AND LIMITATIONS

Please report any bugs or feature requests through the repository at
[https://gitlab.com/polettix/nayme](https://gitlab.com/polettix/nayme).

# AUTHOR

Flavio Poletti.

Embedded specifications/templates, as well as possible expansion for template
fragments, by Christopher Wellons and others
([https://github.com/skeeto/fantasyname](https://github.com/skeeto/fantasyname)).

# LICENSE AND COPYRIGHT

Copyright 2021 by Flavio Poletti `flavio@polettix.it`.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[Kubernetes]: https://kubernetes.io/
[Prometheus]: https://prometheus.io/
[ISO 8601]: https://en.wikipedia.org/wiki/ISO_8601
[Mojolicious]: https://metacpan.org/pod/Mojolicious
